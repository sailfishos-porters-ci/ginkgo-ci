# SailfishOS for Xiaomi Note 8


This repository uses gitlab-ci to build the sailfish images for the Xiaomi Note 8


# Download

[Download the latest build](https://gitlab.com/sailfishos-porters-ci/ginkgo-ci/-/jobs?scope=finished)

# Source code
[https://github.com/sailfish-on-ginkgo](https://github.com/sailfish-on-ginkgo)
